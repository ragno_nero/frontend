import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import ElementService from './services/elementService';
import FighterService from './services/fightService';

class FightersView extends View {
  elementService: ElementService;
  constructor(fighters: Fighter[]) {
    super();
    this.elementService = new ElementService();
    this.handleFighterClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  createFighters(fighters: Fighter[]) {
    const fighterElements = fighters.map((fighter: Fighter) => {
      const fighterView = new FighterView(fighter, this.handleFighterClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    
    this.element.append(...fighterElements);

    const button = this.elementService.getButton("Start",-1);
    button.addEventListener('click', (event: any) => this.handleClick(event), false);
    const root = document.getElementById("root");
    root.append(button);
  }

  async handleFighterClick(event: any, fighter: Fighter): Promise<void> {
    if(!this.fightersDetailsMap.has(fighter._id))
      {
        fighter = await fighterService.getFighterDetails(fighter._id); 
        this.fightersDetailsMap.set(fighter._id, fighter);
      }
      fighter = this.fightersDetailsMap.get(fighter._id);
    if(this.element.children[fighter._id-1].children[0].hasChildNodes())
      document.getElementsByClassName("modal-window")[fighter._id-1].children[0].remove();
    else
      this.element.children[fighter._id-1].children[0].append(this.elementService.createModalWindow(fighter));
      let fsd:FighterService = new FighterService(fighter);

    // get from map or load info and add to fightersMap
    // show modal with fighter info
    // allow to edit health and power in this modal
  }

  handleClick(event) {
    const checked = document.querySelectorAll(".checkbox:checked");
    if(checked.length<2) alert("Need more players. You must check 2 players");
    else if(checked.length>2) alert("Too much players. You must check 2 players");
    else {
      document.getElementById("root").style.display = "none";
      document.getElementById("fight-field").style.display = "flex";
      document.querySelector("#fight-field .fighters").append(checked[0].parentElement.parentElement,checked[1].parentElement.parentElement);
      checked[0].parentElement.remove();
      checked[1].parentElement.remove()
      let firstName = document.querySelectorAll("#fight-field .fighters .fighter .name")[0].getAttribute("alt");
      let secondName = document.querySelectorAll("#fight-field .fighters .fighter .name")[1].getAttribute("alt");
      
      let firstService = new FighterService(this.fightersDetailsMap.get(firstName));
      let secondService = new FighterService(this.fightersDetailsMap.get(secondName));
      document.querySelector(".line-1").innerHTML = firstService._fighter.health.toString();
      document.querySelector(".line-2").innerHTML = secondService._fighter.health.toString();
      firstService.startFight(secondService);
    }
  }

}

export default FightersView;