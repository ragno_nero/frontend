import View from './view';
import ElementService from './services/elementService';

class FighterView extends View {
  elementService: ElementService;
  constructor(fighter: any, handleClick: any) {
    super();
    this.elementService = new ElementService();
    this.createFighter(fighter, handleClick);
  }

  createFighter(fighter: Fighter, handleClick: any): void {
    const { _id, name, source } = fighter;
    const nameElement = this.createName(name, _id);
    const imageElement = this.createImage(source);
    const container = this.elementService.createCheckbox(name);
    const modal = this.createElement({ tagName: 'div', className: 'modal-window' });
    this.element = this.createElement({ tagName: 'div', className: 'wrapper' });
    
    
    const fighterBlock = this.createElement({ tagName: 'div', className: `fighter` });
    fighterBlock.append(imageElement, nameElement);
    
    fighterBlock.append(container);
    fighterBlock.addEventListener('click', (event: any) => handleClick(event, fighter), false);
    this.element.append(modal, fighterBlock);
  }

  createName(name: string, _id: number): HTMLElement {
    const nameElement = this.createElement({ tagName: 'span', className: "name", attributes: {alt: _id } });
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source: string): HTMLElement {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });

    return imgElement;
  }
}

export default FighterView;
