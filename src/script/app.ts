import FightersView from './fightersView';
import { fighterService } from './services/fightersService';
import ElementService from './services/elementService';
import FighterService from './services/fightService';

class App {
  elementService: ElementService;
  constructor() {
    this.elementService = new ElementService()
    this.startApp();

  }

  static rootElement: HTMLElement | any = document.getElementById('root');
  static loadingElement: HTMLElement | any = document.getElementById('loading-overlay');

  async startApp() {
    try {
      App.loadingElement.style.visibility = 'visible';
      
      const fighters = await fighterService.getFighters();
      const fightersView = new FightersView(fighters);
      const fightersElement = fightersView.element;
      App.rootElement.appendChild(fightersElement);
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }

  
}

export default App;