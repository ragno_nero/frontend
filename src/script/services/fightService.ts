class FightService {
    _fighter: Fighter;

    constructor(fighter: Fighter){
        this._fighter = fighter;
    }

    getPowerHit() : number {
        return this._fighter.attack * this.getRandom();
    }
    getBlockPower() : number {
        return this._fighter.defense * this.getRandom();
    }

    getRandom(): number {
        return  Math.round(Math.random()*100)%2 +1;
    }

    startFight(fighter2 :FightService){
        while(this._fighter.health>0&&fighter2._fighter.health>0){
            fighter2._fighter.health -= this.getPowerHit() - fighter2.getBlockPower();
            document.querySelector(".line-2").innerHTML = fighter2._fighter.health.toString();

            this._fighter.health -= fighter2.getPowerHit() - this.getBlockPower();
            console.log(fighter2._fighter.health);
            document.querySelector(".line-1").innerHTML = this._fighter.health.toString();

        }
        if(this._fighter.health>0)
            document.querySelector(".message").innerHTML = "Player 2 win!";
        else
            document.querySelector(".message").innerHTML = "Player 1 win!";
        document.querySelector(".fighters").remove();
            
    }
}

export default FightService;