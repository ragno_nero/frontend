class ElementService {
    element: HTMLElement;
    constructor(){
    }
    
    createElement({ tagName="", className = '', attributes = {} }): HTMLElement {
      const element = document.createElement(tagName);
      if(className != '')
        element.classList.add(className);
      Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));
  
      return element;
    }

    createCheckbox(name: string): HTMLElement{
        const container = this.createElement({ tagName: 'div', className: 'container' });
        const label = this.getLabel(name, false);
        const attributes = { type: "checkbox", id: `fighter-${name}` };
        const checkbox = this.createElement({
          tagName: 'input',
          className: 'checkbox',
          attributes
          });
    
        container.append(checkbox, label);
        
        return container;
      }

      createModalWindow(fighter: Fighter): HTMLElement{
        const form = this.createElement({ tagName: 'form' });  
        const idContainer = this.getContainer("id", "text", fighter, fighter._id, "Id: ", "readonly");
        const nameContainer = this.getContainer("name", "text", fighter, fighter.name, "Name: ");
        const healthContainer = this.getContainer("health", "number", fighter, fighter.health, "Health: ");
        const attackContainer = this.getContainer("attack", "number", fighter, fighter.attack, "Attack: ");
        const defenseContainer = this.getContainer("defense", "number", fighter, fighter.defense, "Defense: ");
        const sourceContainer = this.getContainer("source", "text", fighter, fighter.source, "Source: ")
        const buttonContainer = this.getButton("Save", fighter._id);
        buttonContainer.addEventListener('click', (event: any) => this.handleClick(event, fighter), false);
        form.append(idContainer, nameContainer, healthContainer, attackContainer, defenseContainer, sourceContainer, buttonContainer);
        
        return form;
      }

      handleClick(event, fighter: Fighter){
        const modal = document.getElementsByClassName("modal-window")[fighter._id-1].children[0];
        fighter.name = (<HTMLInputElement>(modal.children[1].children[1])).value;
        fighter.health = Number.parseInt((<HTMLInputElement>(modal.children[2].children[1])).value);
        fighter.attack = Number.parseInt((<HTMLInputElement>modal.children[3].children[1]).value);
        fighter.defense = Number.parseInt((<HTMLInputElement>modal.children[4].children[1]).value);
        fighter.source = (<HTMLInputElement>modal.children[5].children[1]).value;
        document.getElementsByClassName("modal-window")[fighter._id-1].children[0].remove();
  
      }
    
      getContainer(field: string, type: string, fighter: Fighter, value: string|number, title: string, read: string = ""): HTMLElement {
        const formContainerId = this.createElement({ tagName: 'div'});
        const idLabel = this.getLabel(field+"-"+fighter._id, true, title);
        const idInput = this.getInput(type, fighter._id, fighter.name, value, read);
    
        formContainerId.append(idLabel,idInput);
        
        return formContainerId;
      }
    
      getLabel(name: string, flag: boolean, labelName: string = ""): HTMLElement {
        const label = this.createElement({ tagName: 'label', className: "labels", attributes: {for: `fighter-${name}`}});
        if(flag){
            label.innerText = labelName;
        }
        return label;
      }

      getInput(elType:string, _id: number, name: string, value: string|number, readon: string = ""): HTMLElement{
    
        return this.createElement({tagName: "input", className: "elem", attributes: {
            type: elType,
            id: name+`${_id}`,
            value: value,
            readon
        }});
      }

      getButton(name: string, id: number): HTMLElement{
        const formContainerId = this.createElement({ tagName: 'div'});
        
        const btn = this.createElement({tagName: "input", className: "elem", attributes: {
            type: "button",
            value: name,
            name: name,
            id: name
           
        }});
        formContainerId.append(btn);
        return formContainerId;
      }
}
export default ElementService;