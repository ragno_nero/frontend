class Fighter {
    _id: number;
    name: string;
    health: number;
    attack: number;
    defense: number;
    source: string;

    constructor(_id: number, name: string, health: number, attack: number,
                defense: number, source: string) {
        this._id = _id;
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.defense = defense;
        this.source = source;    
    }
}
